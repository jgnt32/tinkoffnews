package com.timewastingguru.domain.network;

import com.timewastingguru.domain.entities.News;

import java.util.List;

public interface NewsRepository {

    List<News> getNewsHeadlines() throws Exception;

    News getNews(int newsId) throws Exception;

}
