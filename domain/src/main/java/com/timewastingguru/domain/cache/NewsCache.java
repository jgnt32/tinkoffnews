package com.timewastingguru.domain.cache;

import com.timewastingguru.domain.entities.News;

import java.util.List;

public interface NewsCache {

    List<News> getNewsHeadlines();

    News getNews(int id);

    void put(List<News> news);

    void put(News news);

}
