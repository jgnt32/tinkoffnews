package com.timewastingguru.domain.interactor;

import com.timewastingguru.domain.cache.NewsCache;
import com.timewastingguru.domain.entities.News;

import java.util.List;
import java.util.concurrent.Executor;

public class GetLocalNewsListInteractor extends ParamlessInteractor<List<News>> {

    private final NewsCache newsCache;

    public GetLocalNewsListInteractor(NewsCache newsCache, Executor executor, Executor postExecutor) {
        super(executor, postExecutor);
        this.newsCache = newsCache;
    }

    @Override
    protected List<News> execute() throws Exception{
        return newsCache.getNewsHeadlines();
    }
}
