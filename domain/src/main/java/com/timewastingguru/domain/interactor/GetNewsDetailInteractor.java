package com.timewastingguru.domain.interactor;

import com.timewastingguru.domain.cache.NewsCache;
import com.timewastingguru.domain.entities.News;
import com.timewastingguru.domain.network.NewsRepository;

import java.util.concurrent.Executor;

public class GetNewsDetailInteractor extends ParameterizedInteractor<Integer, News> {

    private final NewsRepository newsRepository;
    private final NewsCache newsCache;

    public GetNewsDetailInteractor(NewsRepository newsRepository, NewsCache newsCache,
                                   Executor executor, Executor postExecutor) {
        super(executor, postExecutor);
        this.newsRepository = newsRepository;
        this.newsCache = newsCache;
    }

    @Override
    protected News execute(Integer newsId) throws Exception {
        News news = newsRepository.getNews(newsId);
        newsCache.put(news);
        return news;
    }

}
