package com.timewastingguru.domain.interactor;


import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;

public abstract class Interactor<Result> {

    private final Executor executor;
    private final Executor postExecutor;

    public Interactor(Executor executor, Executor postExecutor) {
        this.executor = executor;
        this.postExecutor = postExecutor;
    }

    protected void executeFunction(final Callable<Result> callable, final Listener<Result> listener) {
        executor.execute(new Runnable() {
            @Override
            public void run() {

                WeakReference<Listener<Result>> listenerWeakReference = new WeakReference<>(listener);

                try {
                    final Result result = callable.invoke();

                    final Listener<Result> resultListener = listenerWeakReference.get();
                    if (resultListener != null) {

                        postExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                resultListener.onSuccess(result);
                            }
                        });
                    }

                } catch (final Exception e) {

                    final Listener<Result> resultListener = listenerWeakReference.get();

                    if (resultListener != null) {

                        postExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                resultListener.onError(e);
                            }
                        });
                    }

                }
            }
        });
    }


    protected interface Callable<Result> {

        Result invoke() throws Exception;

    }


    public interface Listener<Result> {

        void onSuccess(Result result);

        void onError(Exception e);

    }

}
