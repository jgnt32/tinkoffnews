package com.timewastingguru.domain.interactor;

import com.timewastingguru.domain.cache.NewsCache;
import com.timewastingguru.domain.entities.News;
import com.timewastingguru.domain.network.NewsRepository;

import java.util.List;
import java.util.concurrent.Executor;

public class GetNewsListInteractor extends ParamlessInteractor<List<News>> {

    private final NewsRepository newsRepository;
    private final NewsCache newsCache;

    public GetNewsListInteractor(NewsRepository newsRepository, NewsCache newsCache, Executor executor, Executor postExecutor) {
        super(executor, postExecutor);
        this.newsRepository = newsRepository;
        this.newsCache = newsCache;
    }

    @Override
    protected List<News> execute() throws Exception{
        List<News> newsHeadlines = newsRepository.getNewsHeadlines();
        newsCache.put(newsHeadlines);
        return newsHeadlines;
    }
}
