package com.timewastingguru.domain.interactor;

import java.util.concurrent.Executor;

public abstract class ParameterizedInteractor<Param, Result> extends Interactor<Result> {

    public ParameterizedInteractor(Executor executor, Executor postExecutor) {
        super(executor, postExecutor);
    }

    public void execute(final Param param, Listener<Result> listener) {
        executeFunction(new Callable<Result>() {
            @Override
            public Result invoke() throws Exception {
                return execute(param);
            }
        }, listener);
    }

    protected abstract Result execute(Param param) throws Exception;

}
