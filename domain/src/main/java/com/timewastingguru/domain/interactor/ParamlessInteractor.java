package com.timewastingguru.domain.interactor;

import java.util.concurrent.Executor;

public abstract class ParamlessInteractor<Result> extends Interactor<Result> {

    public ParamlessInteractor(Executor executor, Executor postExecutor) {
        super(executor, postExecutor);
    }

    public void execute(Listener<Result> listener) {
        executeFunction(new Callable<Result>() {
            @Override
            public Result invoke() throws Exception {
                return execute();
            }
        }, listener);
    }

    protected abstract Result execute() throws Exception;

}
