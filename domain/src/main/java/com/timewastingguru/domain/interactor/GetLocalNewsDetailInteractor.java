package com.timewastingguru.domain.interactor;

import com.timewastingguru.domain.cache.NewsCache;
import com.timewastingguru.domain.entities.News;

import java.util.concurrent.Executor;

public class GetLocalNewsDetailInteractor extends ParameterizedInteractor<Integer, News> {

    private final NewsCache newsCache;

    public GetLocalNewsDetailInteractor(NewsCache newsCache,
                                        Executor executor, Executor postExecutor) {
        super(executor, postExecutor);
        this.newsCache = newsCache;
    }

    @Override
    protected News execute(Integer newsId) throws Exception {
        return newsCache.getNews(newsId);
    }
}
