package com.timewastingguru.data.cache.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.timewastingguru.domain.cache.NewsCache;
import com.timewastingguru.domain.entities.News;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SqliteNewsCache implements NewsCache {

    private final SQLiteDatabase database;

    public SqliteNewsCache(SQLiteDatabase database) {
        this.database = database;
    }

    @Override
    public List<News> getNewsHeadlines() {

        Cursor cursor = database.rawQuery("SELECT * FROM " + NewsTable.NAME + " ORDER BY " + NewsTable.PUBLICATION_DATE + " DESC", null);

        List<News> result = new ArrayList<>(cursor.getCount());

        while (cursor.moveToNext()) {
            News news = new News();
            news.setId(cursor.getInt(cursor.getColumnIndex(NewsTable.ID)));
            news.setTitle(cursor.getString(cursor.getColumnIndex(NewsTable.TITLE)));
            news.setText(cursor.getString(cursor.getColumnIndex(NewsTable.TEXT)));
            news.setPublicationDate(new Date(cursor.getLong(cursor.getColumnIndex(NewsTable.PUBLICATION_DATE))));

            result.add(news);
        }

        cursor.close();
        return result;
    }

    @Override
    public News getNews(int id) {
        Cursor cursor = database.rawQuery("SELECT * FROM " + NewsTable.NAME +
                " WHERE " + NewsTable.ID + " = " + id +
                " ORDER BY " + NewsTable.PUBLICATION_DATE + " DESC", null);
        News result = null;

        if (cursor.moveToFirst()) {
            result = new News();
            result.setId(cursor.getInt(cursor.getColumnIndex(NewsTable.ID)));
            result.setTitle(cursor.getString(cursor.getColumnIndex(NewsTable.TITLE)));
            result.setText(cursor.getString(cursor.getColumnIndex(NewsTable.TEXT)));
            result.setPublicationDate(new Date(cursor.getLong(cursor.getColumnIndex(NewsTable.PUBLICATION_DATE))));
        }

        cursor.close();
        return result;
    }

    @Override
    public void put(List<News> news) {
        database.beginTransaction();

        for (News newsFromRemote : news) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(NewsTable.ID, newsFromRemote.getId());
            contentValues.put(NewsTable.TITLE, newsFromRemote.getTitle());
            contentValues.put(NewsTable.TEXT, newsFromRemote.getText());
            contentValues.put(NewsTable.PUBLICATION_DATE, newsFromRemote.getPublicationDate().getTime());
            database.insertWithOnConflict(NewsTable.NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        }

        database.setTransactionSuccessful();
        database.endTransaction();
    }

    @Override
    public void put(News news) {
        ContentValues contentValues = new ContentValues();
        database.beginTransaction();
        contentValues.put(NewsTable.ID, news.getId());
        contentValues.put(NewsTable.TITLE, news.getTitle());
        contentValues.put(NewsTable.PUBLICATION_DATE, news.getPublicationDate().getTime());
        contentValues.put(NewsTable.TEXT, news.getText());
        database.insertWithOnConflict(NewsTable.NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        database.setTransactionSuccessful();
        database.endTransaction();
    }
}
