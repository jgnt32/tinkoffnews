package com.timewastingguru.data.cache.db;

public class NewsTable {

    public static final String NAME = "news";

    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String TEXT = "text";
    public static final String PUBLICATION_DATE = "date";

    public static String createTable() {

        return "CREATE TABLE " + NAME + "(" +
                ID + " INTEGER PRIMARY KEY, " +
                TITLE + " TEXT, " +
                TEXT + " TEXT, " +
                PUBLICATION_DATE + " LONG);";
    }

}
