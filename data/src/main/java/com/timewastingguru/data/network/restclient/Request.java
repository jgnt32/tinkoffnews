package com.timewastingguru.data.network.restclient;

import java.util.HashMap;
import java.util.Map;

public class Request {

    private String endpoint;
    private String path;
    private Map<String, String> queryParams;
    private Map<String, String> headers;

    private Request() {
    }

    Request(Builder builder) {
        this.endpoint = builder.endpoint;
        this.path = builder.path.toString();
        this.queryParams = builder.queryParams;
        this.headers = builder.headers;
    }

    public String getEndpoint(){
        return endpoint;
    }

    public String getPath() {
        return path;
    }

    public Map<String, String> getQueryParams() {
        return queryParams;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public static class Builder {

        private String endpoint;
        private StringBuilder path = new StringBuilder();
        private Map<String, String> queryParams = new HashMap<>();
        private Map<String, String> headers = new HashMap<>();

        public Builder(String endpoint) {
            this.endpoint = endpoint;
        }

        public Builder concatPath(String path) {
            this.path.append(path);
            return this;
        }

        public Builder addQueryParams(String key, String value) {
            this.queryParams.put(key, value);
            return this;
        }

        public Builder addHeader(String key, String value) {
            this.headers.put(key, value);
            return this;
        }

        public Request build() {
            return new Request(this);
        }

    }

}
