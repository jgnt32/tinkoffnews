package com.timewastingguru.data.network.urlconnectionrestclient;

import com.timewastingguru.data.network.restclient.Request;
import com.timewastingguru.data.network.restclient.RestClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * На реализацию добаления заголовков и POST запросов не хватило времени
 *
 */
public class UrlRestClient implements RestClient {

    @Override
    public String execute(Request request) throws Exception{
        URL url;
        BufferedReader reader = null;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            url = buildUrl(request);
            URLConnection con = url.openConnection();
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return stringBuilder.toString();

    }

    private URL buildUrl(Request request) throws MalformedURLException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(request.getEndpoint());
        stringBuilder.append(request.getPath());
        Map<String, String> params = request.getQueryParams();

        if (params != null && !params.isEmpty()) {
            Iterator<Map.Entry<String,String>> entries = params.entrySet().iterator();
            stringBuilder.append("?");
            while (entries.hasNext()) {
                Map.Entry<String, String> next = entries.next();
                stringBuilder.append(next.getKey()).append("=").append(next.getValue());
                if (entries.hasNext()){
                    stringBuilder.append("&");
                }
            }
        }
        return new URL(stringBuilder.toString());
    }


}
