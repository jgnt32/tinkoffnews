package com.timewastingguru.data.network.restclient;

public interface RestClient {

    String execute(Request request) throws Exception;

}
