package com.timewastingguru.data.network;


import com.timewastingguru.data.network.restclient.Request;
import com.timewastingguru.data.network.restclient.RestClient;
import com.timewastingguru.domain.entities.News;
import com.timewastingguru.domain.network.NewsRepository;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UrlConnectionNewsRepo implements NewsRepository {

    public static final String ENDPOINT = "https://api.tinkoff.ru";
    private final RestClient restClient;

    public UrlConnectionNewsRepo(RestClient restClient) {
        this.restClient = restClient;
    }

    @Override
    public List<News> getNewsHeadlines() throws Exception {

        List<News> result = null;

        Request request = new Request.Builder(ENDPOINT).concatPath("/v1/news").build();
        String requestBody = restClient.execute(request);

        JSONObject jsonObject = new JSONObject(requestBody);
        JSONArray payload = jsonObject.getJSONArray("payload");

        if (payload.length() > 0) {
            result = new ArrayList<>(payload.length());

            for (int i = 0; i < payload.length(); i++) {
                JSONObject headline = payload.getJSONObject(i);
                int id = headline.getInt("id");
                String title = headline.getString("text");
                Date date = new Date(headline.getJSONObject("publicationDate").getLong("milliseconds"));
                News news = new News(id, title, date);
                result.add(news);
            }
        }

        return result;
    }

    @Override
    public News getNews(int newsId) throws Exception {
        Request request = new Request.Builder(ENDPOINT)
                .concatPath("/v1/news_content")
                .addQueryParams("id", Integer.toString(newsId))
                .build();
        String requestBody = restClient.execute(request);
        JSONObject jsonObject = new JSONObject(requestBody);
        JSONObject payload = jsonObject.getJSONObject("payload");
        JSONObject headline = payload.getJSONObject("title");
        Date date = new Date(headline.getJSONObject("publicationDate").getLong("milliseconds"));
        return new News(headline.getInt("id"), headline.getString("text"), payload.getString("content"), date);
    }

}
