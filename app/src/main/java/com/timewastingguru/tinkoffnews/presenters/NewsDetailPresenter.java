package com.timewastingguru.tinkoffnews.presenters;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.timewastingguru.domain.entities.News;
import com.timewastingguru.domain.interactor.GetLocalNewsDetailInteractor;
import com.timewastingguru.domain.interactor.GetNewsDetailInteractor;
import com.timewastingguru.domain.interactor.Interactor;
import com.timewastingguru.tinkoffnews.views.NewsDetailView;
import com.timewastingguru.tinkoffnews.views.utils.ObservableField;

public class NewsDetailPresenter extends BasePresnter<NewsDetailView> {

    private final int newsId;
    private final GetNewsDetailInteractor newsDetailInteractor;
    private final GetLocalNewsDetailInteractor localNewsDetailInteractor;

    private final ObservableField<Exception> error = new ObservableField<>();
    private final ObservableField<News> news = new ObservableField<>();

    public NewsDetailPresenter(NewsDetailView view, int newsId, GetNewsDetailInteractor newsDetailInteractor,
                               GetLocalNewsDetailInteractor localNewsDetailInteractor) {
        super(view);
        this.newsId = newsId;
        this.newsDetailInteractor = newsDetailInteractor;
        this.localNewsDetailInteractor = localNewsDetailInteractor;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            newsDetailInteractor.execute(newsId, new Interactor.Listener<News>() {
                @Override
                public void onSuccess(News news) {
                    loadNewsFromCache();
                    error.setValue(null);
                }

                @Override
                public void onError(Exception e) {
                    error.setValue(e);
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        loadNewsFromCache();
    }

    private void loadNewsFromCache() {
        localNewsDetailInteractor.execute(newsId, new Interactor.Listener<News>() {
            @Override
            public void onSuccess(News news) {
                NewsDetailPresenter.this.news.setValue(news);
            }

            @Override
            public void onError(Exception e) {

            }
        });
    }

    public ObservableField<Exception> getError() {
        return error;
    }

    public ObservableField<News> getNews() {
        return news;
    }
}
