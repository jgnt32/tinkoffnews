package com.timewastingguru.tinkoffnews.presenters;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.timewastingguru.tinkoffnews.views.BaseView;

public abstract class BasePresnter<V extends BaseView> {

    private final V view;

    public BasePresnter(V view) {
        this.view = view;
    }

    protected V getView() {
        return view;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {

    }

    public void onViewCreated() {

    }

    public void onStart() {

    }

    public void onStop() {
    }

    public void onSaveInstanceState(Bundle state) {

    }

    public void onRestoreInstanceState(Bundle state) {

    }

    public void onDestroy() {

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){

    }

}
