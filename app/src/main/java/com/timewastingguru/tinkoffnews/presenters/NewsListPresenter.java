package com.timewastingguru.tinkoffnews.presenters;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;

import com.timewastingguru.domain.entities.News;
import com.timewastingguru.domain.interactor.GetLocalNewsListInteractor;
import com.timewastingguru.domain.interactor.GetNewsListInteractor;
import com.timewastingguru.domain.interactor.Interactor;
import com.timewastingguru.tinkoffnews.views.NewsListView;
import com.timewastingguru.tinkoffnews.views.utils.ObservableField;

import java.util.ArrayList;
import java.util.List;

public class NewsListPresenter extends BasePresnter<NewsListView>{

    private final GetNewsListInteractor newsInteractor;
    private final GetLocalNewsListInteractor localNewsInteractor;

    private final List<News> news = new ArrayList<>();

    private final ObservableField<Exception> error = new ObservableField<>();
    private final ObservableField<DiffUtil.DiffResult> diffResult = new ObservableField<>();

    public NewsListPresenter(NewsListView view, GetNewsListInteractor newsInteractor,
                             GetLocalNewsListInteractor localNewsInteractor) {
        super(view);
        this.newsInteractor = newsInteractor;
        this.localNewsInteractor = localNewsInteractor;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            loadFromRemote();
        }

    }

    private void loadFromRemote() {
        newsInteractor.execute(new Interactor.Listener<List<News>>() {
            @Override
            public void onSuccess(List<News> news) {
                System.out.println(news);
                loadNewsFromCache();
            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                NewsListPresenter.this.error.setValue(e);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();


        loadNewsFromCache();
    }

    private void loadNewsFromCache() {
        localNewsInteractor.execute(new Interactor.Listener<List<News>>() {
            @Override
            public void onSuccess(final List<News> news) {
                System.out.println(news);
                DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                    @Override
                    public int getOldListSize() {
                        return  NewsListPresenter.this.news.size();
                    }

                    @Override
                    public int getNewListSize() {
                        return news.size();
                    }

                    @Override
                    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                        return NewsListPresenter.this.news.get(oldItemPosition).getId() == news.get(newItemPosition).getId();
                    }

                    @Override
                    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                        return  NewsListPresenter.this.news.get(oldItemPosition).equals(news.get(newItemPosition));
                    }
                }, false);
                NewsListPresenter.this.news.clear();
                NewsListPresenter.this.news.addAll(news);
                NewsListPresenter.this.diffResult.setValue(diffResult);
            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void onForceRefresh() {
        loadFromRemote();
    }

    public List<News> getNews() {
        return news;
    }

    public ObservableField<Exception> getError() {
        return error;
    }

    public ObservableField<DiffUtil.DiffResult> getDiffResult() {
        return diffResult;
    }
}
