package com.timewastingguru.tinkoffnews.internal.di;

import android.database.sqlite.SQLiteOpenHelper;

import com.timewastingguru.data.cache.db.SqliteNewsCache;
import com.timewastingguru.domain.cache.NewsCache;

public class NewsCacheFactory {

    private static SQLiteOpenHelper openHelper;
    private static SqliteNewsCache sqliteNewsCache;

    public static void setOpenHelper(SQLiteOpenHelper openHelper) {
        NewsCacheFactory.openHelper = openHelper;
    }

    public static NewsCache getNewsCache() {
        if (sqliteNewsCache == null) { // singletone
            sqliteNewsCache = new SqliteNewsCache(openHelper.getWritableDatabase());
        }
        return sqliteNewsCache;
    }

}
