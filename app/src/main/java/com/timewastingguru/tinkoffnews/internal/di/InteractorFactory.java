package com.timewastingguru.tinkoffnews.internal.di;

import com.timewastingguru.domain.interactor.GetLocalNewsDetailInteractor;
import com.timewastingguru.domain.interactor.GetLocalNewsListInteractor;
import com.timewastingguru.domain.interactor.GetNewsDetailInteractor;
import com.timewastingguru.domain.interactor.GetNewsListInteractor;

public class InteractorFactory {

    public static GetNewsListInteractor getNewsInteractor() {
        return new GetNewsListInteractor(NewsRepositoryFactory.getNetworkRepository(),
                NewsCacheFactory.getNewsCache(),
                ThreatExecutorFactory.getExecutor(), PostExecutorFactory.getPostExecutor());
    }

    public static GetLocalNewsListInteractor getLocalNewsInteractor() {
        return new GetLocalNewsListInteractor(NewsCacheFactory.getNewsCache(),
                ThreatExecutorFactory.getExecutor(), PostExecutorFactory.getPostExecutor());
    }

    public static GetNewsDetailInteractor getNewsDetailInteractor() {
        return new GetNewsDetailInteractor(NewsRepositoryFactory.getNetworkRepository(), NewsCacheFactory.getNewsCache(),
                ThreatExecutorFactory.getExecutor(), PostExecutorFactory.getPostExecutor());
    }

    public static GetLocalNewsDetailInteractor getLocalNewsDetailInteractor() {
        return new GetLocalNewsDetailInteractor(NewsCacheFactory.getNewsCache(),
                ThreatExecutorFactory.getExecutor(), PostExecutorFactory.getPostExecutor());
    }

}
