package com.timewastingguru.tinkoffnews.internal.di;

import com.timewastingguru.data.network.restclient.RestClient;
import com.timewastingguru.data.network.urlconnectionrestclient.UrlRestClient;

public class RestClientFactory {

    public static RestClient getRestClient() {
        return new UrlRestClient();
    }

}
