package com.timewastingguru.tinkoffnews.internal.di;

import com.timewastingguru.data.network.UrlConnectionNewsRepo;
import com.timewastingguru.domain.network.NewsRepository;

public class NewsRepositoryFactory {

    private static UrlConnectionNewsRepo urlConnectionNewsRepo;

    public static NewsRepository getNetworkRepository() {
        if (urlConnectionNewsRepo == null) {
            urlConnectionNewsRepo = new UrlConnectionNewsRepo(RestClientFactory.getRestClient());
        }
        return urlConnectionNewsRepo;
    }

}
