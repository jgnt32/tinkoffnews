package com.timewastingguru.tinkoffnews.internal.di;

import android.os.Handler;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;

public class PostExecutorFactory {

    private static AndroidMainThreadExecutor androidMainThreadExecutor = new AndroidMainThreadExecutor();

    public static Executor getPostExecutor() {
        return androidMainThreadExecutor;
    }


    private static class AndroidMainThreadExecutor implements Executor {

        Handler handler = new Handler();

        @Override
        public void execute(@NonNull Runnable runnable) {
            handler.post(runnable);
        }
    }

}
