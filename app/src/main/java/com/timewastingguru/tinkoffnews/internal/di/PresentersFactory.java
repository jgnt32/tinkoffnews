package com.timewastingguru.tinkoffnews.internal.di;

import com.timewastingguru.tinkoffnews.presenters.NewsDetailPresenter;
import com.timewastingguru.tinkoffnews.presenters.NewsListPresenter;
import com.timewastingguru.tinkoffnews.views.NewsDetailView;
import com.timewastingguru.tinkoffnews.views.NewsListView;

public class PresentersFactory {

    public static NewsListPresenter getNewsPresenter(NewsListView view) {
        return new NewsListPresenter(view, InteractorFactory.getNewsInteractor(), InteractorFactory.getLocalNewsInteractor());
    }

    public static NewsDetailPresenter getNewsDetailPresenter(NewsDetailView newsDetailView, int newsId) {
        return new NewsDetailPresenter(newsDetailView, newsId, InteractorFactory.getNewsDetailInteractor(),
                InteractorFactory.getLocalNewsDetailInteractor());
    }

}
