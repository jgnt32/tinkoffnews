package com.timewastingguru.tinkoffnews.views.ftagments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.timewastingguru.domain.entities.News;
import com.timewastingguru.tinkoffnews.R;
import com.timewastingguru.tinkoffnews.internal.di.PresentersFactory;
import com.timewastingguru.tinkoffnews.presenters.NewsDetailPresenter;
import com.timewastingguru.tinkoffnews.views.BaseFragment;
import com.timewastingguru.tinkoffnews.views.NewsDetailView;
import com.timewastingguru.tinkoffnews.views.utils.HtmlCompat;
import com.timewastingguru.tinkoffnews.views.utils.ObservableField;

import java.text.SimpleDateFormat;

public class NewsDetailFragment extends BaseFragment<NewsDetailPresenter> implements NewsDetailView {

    public static final String NEWS_ID = "news_id";

    private TextView title;
    private TextView date;
    private TextView text;
    private View emptyView;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm dd MMMM 2017");
    private ProgressBar progressBar;
    private TextView errorMessage;

    public static NewsDetailFragment newFragment(int id) {
        NewsDetailFragment result = new NewsDetailFragment();
        Bundle args = new Bundle();
        args.putInt(NEWS_ID, id);
        result.setArguments(args);
        return result;
    }

    @Override
    protected void inject() {
        if (getPresenter() == null) { // cause its retain
            setPresenter(PresentersFactory.getNewsDetailPresenter(this, getArguments().getInt(NEWS_ID)));
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.news_detail_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        title = view.findViewById(R.id.title);
        date = view.findViewById(R.id.date);
        text = view.findViewById(R.id.text);
        emptyView = view.findViewById(R.id.empty_view);
        progressBar = view.findViewById(R.id.progress_bar);
        errorMessage = view.findViewById(R.id.error_message);
        getPresenter().getNews().setListener(new ObservableField.PropertyChangeListener<News>() {
            @Override
            public void onPropertyChanged(News value) {
                bindViews(getPresenter().getNews().getValue(), getPresenter().getError().getValue());
            }
        });

        getPresenter().getError().setListener(new ObservableField.PropertyChangeListener<Exception>() {
            @Override
            public void onPropertyChanged(Exception e) {
                bindViews(getPresenter().getNews().getValue(), getPresenter().getError().getValue());
            }
        });

    }

    public void bindViews(@Nullable News news, @Nullable Exception e) {

        if (news != null) {
            title.setText(HtmlCompat.fromHtml(news.getTitle()));
            date.setText(simpleDateFormat.format(news.getPublicationDate()));
            if (news.getText() != null) {
                text.setText(HtmlCompat.fromHtml(news.getText()));
            }
        }

        errorMessage.setVisibility(e != null && news != null && news.getText() == null ? View.VISIBLE : View.GONE);
        emptyView.setVisibility(e != null && news != null && news.getText() == null ? View.VISIBLE : View.GONE);
        progressBar.setVisibility(e == null && news != null && news.getText() == null ? View.VISIBLE : View.GONE);
    }

}
