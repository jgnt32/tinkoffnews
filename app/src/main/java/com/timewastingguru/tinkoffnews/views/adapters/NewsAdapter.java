package com.timewastingguru.tinkoffnews.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.timewastingguru.domain.entities.News;
import com.timewastingguru.tinkoffnews.R;

import java.text.SimpleDateFormat;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder>{

    private final List<News> news;
    private final OnNewsClickListener onNewsClickListener;

    public NewsAdapter(List<News> news, OnNewsClickListener onNewsClickListener) {
        this.news = news;
        this.onNewsClickListener = onNewsClickListener;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_item, parent, false);
        final NewsViewHolder newsViewHolder = new NewsViewHolder(inflate);
        newsViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNewsClickListener.onNewsClick(news.get(newsViewHolder.getAdapterPosition()));
            }
        });
        return newsViewHolder;
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        holder.bind(news.get(position));
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    public static class NewsViewHolder extends RecyclerView.ViewHolder {

        private final SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm dd MMMM yyyy");

        private final TextView title;
        private final TextView date;

        public NewsViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            date = itemView.findViewById(R.id.publication_date);
        }

        public void bind(News news) {
            title.setText(Html.fromHtml(news.getTitle()));
            date.setText(dateFormat.format(news.getPublicationDate()));
        }

    }

    public interface OnNewsClickListener {

        void onNewsClick(News news);

    }

}
