package com.timewastingguru.tinkoffnews.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.timewastingguru.tinkoffnews.R;
import com.timewastingguru.tinkoffnews.views.ftagments.NewsDetailFragment;

public class NewsDetailActivity extends AppCompatActivity {

    public static Intent newIntent(Context context, int newsId) {
        Intent intent = new Intent(context, NewsDetailActivity.class);
        intent.putExtra("news_id", newsId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_detail_activity);
        int newsId = getIntent().getIntExtra("news_id", -1);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, NewsDetailFragment.newFragment(newsId))
                .commit();

    }
}
