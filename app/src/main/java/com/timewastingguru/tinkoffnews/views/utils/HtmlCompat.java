package com.timewastingguru.tinkoffnews.views.utils;

import android.text.Html;
import android.text.Spanned;

public class HtmlCompat {

    public static Spanned fromHtml(String s) {

        if (android.os.Build.VERSION.SDK_INT < 24) {
            return Html.fromHtml(s);
        } else {
            return Html.fromHtml(s, Html.FROM_HTML_MODE_LEGACY);
        }

    }

}
