package com.timewastingguru.tinkoffnews.views.utils;

import android.support.annotation.Nullable;

public class ObservableField<T> {

    private T t;

    private PropertyChangeListener<T> listener;

    public ObservableField() {
    }

    public ObservableField(T t) {
        this.t = t;
    }

    public T getValue() {
        return t;
    }

    public void setValue(T t) {
        this.t = t;
        if (listener != null) {
            listener.onPropertyChanged(t);
        }
    }

    public void setListener(PropertyChangeListener<T> listener) {
        this.listener = listener;
    }

    public interface PropertyChangeListener<T> {

        void onPropertyChanged(@Nullable T t);

    }


}
