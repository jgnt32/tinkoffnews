package com.timewastingguru.tinkoffnews.views.ftagments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.timewastingguru.domain.entities.News;
import com.timewastingguru.tinkoffnews.R;
import com.timewastingguru.tinkoffnews.internal.di.PresentersFactory;
import com.timewastingguru.tinkoffnews.presenters.NewsListPresenter;
import com.timewastingguru.tinkoffnews.views.BaseFragment;
import com.timewastingguru.tinkoffnews.views.NewsListView;
import com.timewastingguru.tinkoffnews.views.activities.NewsDetailActivity;
import com.timewastingguru.tinkoffnews.views.adapters.NewsAdapter;
import com.timewastingguru.tinkoffnews.views.utils.ObservableField;

public class NewsListFragment extends BaseFragment<NewsListPresenter> implements NewsListView, NewsAdapter.OnNewsClickListener {

    private RecyclerView recyclerView;
    private NewsAdapter adapter;
    private View emptyView;
    private ProgressBar progressBar;
    private TextView errorMessage;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void inject() {
        if (getPresenter() == null) { // cause its retain
            setPresenter(PresentersFactory.getNewsPresenter(this));
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new NewsAdapter(getPresenter().getNews(), this);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.news_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycleView);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout = view.findViewById(R.id.swipe_to_refresh_layout);

        emptyView = view.findViewById(R.id.empty_view);
        progressBar = view.findViewById(R.id.progress_bar);
        errorMessage = view.findViewById(R.id.error_message);

        getPresenter().getDiffResult().setListener(new ObservableField.PropertyChangeListener<DiffUtil.DiffResult>() {
            @Override
            public void onPropertyChanged(DiffUtil.DiffResult diffResult) {
                bindViews(getPresenter().getError().getValue());
                diffResult.dispatchUpdatesTo(adapter);
            }
        });

        getPresenter().getError().setListener(new ObservableField.PropertyChangeListener<Exception>() {
            @Override
            public void onPropertyChanged(Exception e) {
                bindViews(e);
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPresenter().onForceRefresh();
            }
        });
    }

    private void bindViews(Exception e) {
        errorMessage.setVisibility(e != null && adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        emptyView.setVisibility(e != null && adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        progressBar.setVisibility(e == null && adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onNewsClick(News news) {
        startActivity(NewsDetailActivity.newIntent(getContext(), news.getId()));
    }

}
