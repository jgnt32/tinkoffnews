package com.timewastingguru.tinkoffnews;

import android.app.Application;

import com.timewastingguru.data.cache.db.DefaultOpenHelper;
import com.timewastingguru.tinkoffnews.internal.di.NewsCacheFactory;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        NewsCacheFactory.setOpenHelper(new DefaultOpenHelper(this));
    }
}
